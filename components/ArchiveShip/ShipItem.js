import React from 'react';
import { Col } from 'react-bootstrap';
import Link from 'next/link';

const getTypeColor = (type) => {
    let color = '';
    switch (type) {
        case 'fire':
            color = 't-orange';
            break;
        case 'acid':
            color = 't-yellow';
            break;
        case 'electro':
            color = 't-violet';
            break;
        case 'ice':
            color = 't-blue';
            break;
        case 'dark':
            color = 't-dark';
            break;
    }

    return color;
};

export default function ShipItem({ col, dataShip }) {
    return (
        <Col lg={col} className="ship-item-wrap">
            <Link href="#">
                <a className="ship-item">
                    <div className="ship-item-header">
                        <div className="ship-id">#{dataShip.id}</div>
                        <div
                            className={`ship-type ${getTypeColor(
                                dataShip.type
                            )}`}
                        >
                            {dataShip.type}
                        </div>
                    </div>
                    <div className="ship-item-content">
                        <div className="ship-body">
                            <img src={dataShip.image} />
                        </div>
                    </div>
                    <div className="ship-item-footer">
                        <div className="ship-stat">
                            <div className="stat-box">
                                <div className="stat-icon">
                                    <img src="/img/ship-detail/weapons.png" />
                                </div>
                                <div className="stat-value">
                                    {dataShip.stats['damage']}
                                </div>
                            </div>
                            <div className="stat-box">
                                <div className="stat-icon">
                                    <img src="/img/ship-detail/turrets.png" />
                                </div>
                                <div className="stat-value">
                                    {dataShip.stats['fire_rate']}
                                </div>
                            </div>
                            <div className="stat-box">
                                <div className="stat-icon">
                                    <img src="/img/ship-detail/armor.png" />
                                </div>
                                <div className="stat-value">
                                    {dataShip.stats['armor']}
                                </div>
                            </div>
                            <div className="stat-box">
                                <div className="stat-icon">
                                    <img src="/img/ship-detail/power-plants.png" />
                                </div>
                                <div className="stat-value">
                                    {dataShip.stats['health']}
                                </div>
                            </div>
                        </div>
                        <div className="ship-name">{dataShip.name}</div>
                    </div>
                </a>
            </Link>
        </Col>
    );
}
