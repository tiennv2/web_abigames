import React from 'react';
import { Row, Pagination } from 'react-bootstrap';
import ShipItem from './ShipItem';

export default function ShipList({
    col,
    dataListShip,
    limit = 12,
    isShowPagination = false,
}) {
    return (
        <div className="ship-list">
            <Row>
                {dataListShip.slice(0, limit).map((ship, index) => (
                    <ShipItem col={col} dataShip={ship} key={index} />
                ))}
            </Row>
            <Row className={isShowPagination ? '' : 'd-none'}>
                <div className="ship-list-pagination">
                    <Pagination>
                        <Pagination.Prev />
                        <Pagination.Item>{1}</Pagination.Item>
                        <Pagination.Item>{2}</Pagination.Item>
                        <Pagination.Item>{3}</Pagination.Item>
                        <Pagination.Item active>{4}</Pagination.Item>
                        <Pagination.Item>{5}</Pagination.Item>
                        <Pagination.Item>{6}</Pagination.Item>
                        <Pagination.Next />
                    </Pagination>
                </div>
            </Row>
        </div>
    );
}
