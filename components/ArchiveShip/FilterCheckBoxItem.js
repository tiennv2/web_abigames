import React from 'react';

export default function FilterCheckBoxItem({ title, icon, titleColorClass }) {
    return (
        <label className="filter-item-root filter-item">
            <span className="filter-item-label">
                <input type="checkbox" />
            </span>
            <span className="filter-item-meta-root">
                <div className="filter-item-meta-wrap">
                    <img className="filter-item-icon" src="" alt="" />
                    <span className={`filter-item-title ${titleColorClass}`}>
                        {title}
                    </span>
                </div>
            </span>
        </label>
    );
}
