import React, { useState, useRef, useEffect, useContext } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AuthContext from '@/context/AuthContext';
import axios from 'axios';

import logo from '/public/images/Logo.svg';
import qrDownload from '/public/img/decoration/qr.png';
import ava from '/public/img/decoration/ava.png';
import logoutImg from '/public/img/decoration/logout.png';

import { Container, DropdownButton, Dropdown, Row, Col } from 'react-bootstrap';

const isLogin = false;
const languages = ['English', 'Français', 'Português', '日本', '한국인', '中文'];

// const AccountBox = () => {
//     if (isLogin) {
//         return (
//             <div className="account-login">
//                 <a href="#" className="account-meta">
//                     <Image src={ava} width={34} height={34} objectFit="cover" />
//                     <div className="account-name">
//                         Unkown name <span className="arrow-down"></span>
//                     </div>
//                 </a>
//                 <ul className="account-box box-transparent">
//                     <li className="account-item">
//                         <Link href="#">
//                             <a className="account-link">
//                                 <span className="link-left">
//                                     <span className="label-line"></span>User Center
//                                 </span>{' '}
//                                 <span className="arrow-right"></span>
//                             </a>
//                         </Link>
//                     </li>
//                     <li className="account-item">
//                         <Link href="#">
//                             <a className="account-link">
//                                 <span className="link-left">
//                                     <span className="label-line"></span>GAR Wallet
//                                 </span>{' '}
//                                 <span className="arrow-right"></span>
//                             </a>
//                         </Link>
//                     </li>
//                     <li className="account-item account-item-last">
//                         <Link href="#">
//                             <a className="account-link">
//                                 <span className="img-logout"><Image src={logout} width={12} objectFit="contain" /></span>
//                                 Log Out
//                             </a>
//                         </Link>
//                     </li>
//                 </ul>
//             </div>
//         );
//     } else {
//         return (
//             <div className="account-not-login">
//                 <Link href="/user/login"><a className="account-btn btn login">Login</a></Link>
//                 <Link href="/user/register"><a className="account-btn active btn register d-none d-xl-block">Register</a></Link>
//             </div>
//         );
//     }
// };


export default function Navbar() {
    const { user, logout } = useContext(AuthContext);

    const [language, setLanguage] = useState('English');
    const navRef = useRef(null);

    const handleScroll = () => {
        if (window.scrollY > 0) {
            navRef.current.classList.add('stuck');
        } else {
            navRef.current.classList.remove('stuck');
        }
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    });


    return (
        <Container fluid className="header" ref={navRef}>
            <ul className="menu-mobile">
                <li className="menu-bars-mobile float-right"><span><FontAwesomeIcon icon={faBars} /></span></li>
            </ul>
            <Row>
                <Col xl={6}>
                    <nav className="navbar navbar-expand-lg">
                        <div className="header-meta">
                            <ul className="menu float-xl-left">
                                <li className="menu-item">
                                    <Link href="/">
                                        <a className="logo">
                                            <Image src={logo} />
                                        </a>
                                    </Link>
                                </li>
                                <li className="menu-item">
                                    <Link href="https://whitepaper.galaxyattack.io/">
                                        <a className="menu-link">ABI Farmer</a>
                                    </Link>
                                </li>
                                <li className="menu-item">
                                    <Link href="/ships">
                                        <a className="menu-link">Game Play</a>
                                    </Link>
                                </li>
                                <li className="menu-item">
                                    <Link href="/ships">
                                        <a className="menu-link">NFT Marketplace</a>
                                    </Link>
                                </li>
                                <li className="menu-item">
                                    <Link href="/ships">
                                        <a className="menu-link deactive">Docs</a>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </Col>
                <Col xl={6}>
                    <div className="header-action ">
                        <ul className="float-xl-right">
                            <li className="action-item action-item d-none d-xl-block">
                                <Link href="#">
                                    <a className="btn-radius btn-coin number text-center">M.Cap {' '} <span className="coin-value">$15,000,000</span></a>
                                </Link>
                            </li>
                            <li className="action-item action-item d-none d-xl-block">
                                <Link href="#">
                                    <a className="btn-radius btn-coin number text-center">BUY ABI <span className="coin-value">$1,00</span></a>
                                </Link>
                            </li>
                            <li className="action-item">
                                <div className="account">
                                    { user ? (
                                        <>
                                            <div className="account-login">
                                                <a href="#" className="account-meta">
                                                    <Image src={ava} width={34} height={34} objectFit="cover" />
                                                    <div className="account-name">
                                                        Unkown name <span className="arrow-down"></span>
                                                    </div>
                                                </a>
                                                <ul className="account-box box-transparent">
                                                    <li className="account-item">
                                                        <Link href="#">
                                                            <a className="account-link">
                                                                <span className="link-left">
                                                                    <span className="label-line"></span>User Center
                                                                </span>{' '}
                                                                <span className="arrow-right"></span>
                                                            </a>
                                                        </Link>
                                                    </li>
                                                    <li className="account-item">
                                                        <Link href="#">
                                                            <a className="account-link">
                                                                <span className="link-left">
                                                                    <span className="label-line"></span>GAR Wallet
                                                                </span>{' '}
                                                                <span className="arrow-right"></span>
                                                            </a>
                                                        </Link>
                                                    </li>
                                                    <li className="account-item account-item-last">
                                                        <Link href="#" onClick={() => logout()}>
                                                            <a className="account-link">
                                                                <span className="img-logout"><Image src={logoutImg} width={12} objectFit="contain" /></span>
                                                                Log Out
                                                            </a>
                                                        </Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        </>
                                        ) : (
                                        <>
                                            <div className="account-not-login">
                                                <Link href=""><a className="account-btn btn login">Login</a></Link>
                                                <Link href=""><a className="account-btn active btn register d-none d-xl-block">Register</a></Link>
                                            </div>
                                        </>
                                    )}
                                    {/* <AccountBox></AccountBox> */}
                                </div>
                            </li>
                            <li className="action-item">
                                <div className="download">Download <span className="arrow-down"></span>
                                    <div className="box-qr box-transparent">
                                        <div className="download-qr">
                                            <Image src={qrDownload} />
                                        </div>
                                        <div className="box-desc">
                                            iOS - Android Download
                                        </div>
                                        <div className="box-line"></div>
                                        <Link href="#">
                                            <a className="box-link">More Download</a>
                                        </Link>
                                    </div>
                                </div>
                            </li>
                            <li className="action-item">
                                <div className="language">
                                    <DropdownButton
                                        align="end"
                                        title={language}
                                        id="dropdown-basic"
                                    >
                                        {languages.map((lang, id) => (
                                            <Dropdown.Item
                                                key={id}
                                                href="#"
                                                onClick={() => {
                                                    setLanguage(lang);
                                                }}
                                                eventKey="1"
                                            >
                                                {lang}
                                            </Dropdown.Item>
                                        ))}
                                    </DropdownButton>
                                </div>
                            </li>
                        </ul>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}

