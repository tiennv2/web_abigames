import React from 'react';
import Navbar from './Navbar';
import Footer from './Footer';
import Head from 'next/head';
import { useRouter } from 'next/router';

function getHeaderPageClassName(routerPathName) {
    let classNameHeader = '';
    switch (routerPathName) {
        case '/':
        case '/ships':
            classNameHeader = 'main-app page-fixed';
            break;

        default:
            classNameHeader = 'main-app page-normal';
            break;
    }
    return classNameHeader;
}

export default function Layout({ children }) {
    const router = useRouter();

    return (
        <div className={getHeaderPageClassName(router.pathname)}>
            <Head>
                <title>GALAXY ATTACK: Revolution</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link rel="stylesheet" type="text/css" charset="UTF-8" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
            </Head>
            <Navbar></Navbar>
            {children}
            <Footer></Footer>
            <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>   
            <script src="/js/snowfall-canvas.js"></script>
        </div>
    );
}
