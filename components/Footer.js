import React, { Component } from 'react';
import { Container, Row, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import footerData from '/public/data/footer';
import titleIconData from '/public/data/iconTitle';

export default function Footer() {
    return (
        <footer className="footer">
            <div className="copy-right">
                <div className="img"><img src="./images/Character-Concept-Group.png"/></div>
                <div className="img-content text-center"><img src="./images/logo-bottom.png"/></div>
                <div className="blog-content text-center">
                    <div className="content">
                        <p className="text">Copyright © 2021 ABI GAME STUDIO. All rights reserved.</p>
                        <p className="text"><span>Terms of Service | Privacy Policy</span></p>
                    </div>
                </div>
            </div>
            <div id="stop" className="scrollTop">
                <span><a><i className="fas fa-chevron-up"></i></a></span>
            </div>
        </footer>
    );
}
