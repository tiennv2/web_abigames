import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import titleIconData from '/public/data/iconTitle';

export default function News() {
    return (
        <div className="block block-gallery">
            <canvas id="snowfall" className="snowfall"></canvas>
            <Container>
                <div className="block-header">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>Free To Play, Play to Earn  </p>
                        <p className="block-subtitle">Gallery</p>
                    </div>
                </div>
                <Row className="justify-content-end">
                    <Col lg={6}>
                        <div className="content wow fadeInUp  animated">
                            <div className="block-title text-center">
                                <p>Free To Play, Play to Earn  </p>
                                <p className="block-subtitle">Our Vision</p>
                            </div>
                            <div className="text">Our vision is that no NFT metaverse should be singular but rather each metaverse can be interconnected giving each unique NFT increased utility through NFT interoperability between games and platforms.</div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
