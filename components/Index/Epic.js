import React, { useState, useRef } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';

import epicData from '../../public/data/epic';

const EpicSlideItem = ({ epic, index, sliderEpicRef }) => {
    const slideRef = useRef(null);
    const deactiveAllSlide = () => {
        sliderEpicRef.current
            .querySelectorAll('.epic-slide-item')
            .forEach((slide, index) => {
                slide.classList.remove('active');
            });
    };

    return (
        <div
            className={
                index === 0 ? 'epic-slide-item active' : 'epic-slide-item'
            }
            onClick={() => {
                deactiveAllSlide();
                slideRef.current.classList.add('active');
            }}
            ref={slideRef}
        >
            <div className="epic-slide-img">
                <div className="epic-slide-bg"></div>
                <div className="epic-slide-head">
                    <img src={epic.image} className="epic-img" />
                    <img
                        src="/img/decoration/epic-shadown.png"
                        className="epic-img-shadown"
                    />
                </div>
            </div>
            <div className="epic-slide-meta">
                <div className="epic-slide-title">{epic.title}</div>
            </div>
        </div>
    );
};

export default function Epic() {
    const [epic, setEpic] = useState(epicData[0]);
    const sliderEpicref = useRef(null);

    return (
        <div className="block block-epic">
            <div className="block-wrapper">
                <img
                    src="/img/decoration/bg2.c8819f39.png"
                    className="light-box light-box-epic-top"
                />
                <Container>
                    <div className="block-header block-header-start">
                        <div className="block-header-right">
                            <div className="border-title"></div>
                            <div className="block-header-meta">
                                <div className="block-title">Legendry NFTs</div>
                                <div className="block-subtitle">
                                    FREE TO PLAY, PLAY TO EARN
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="block-content">
                        <div className="epic-meta">
                            <img
                                src="/img/decoration/epic-bg.png"
                                className="epic-bg"
                            />
                            <Row className="epic-meta-wrap justify-content-center">
                                <Col lg={3}>
                                    <div className="epic-ava">
                                        <img
                                            src={epic.image}
                                            className="epic-ava-main"
                                        />
                                        <img
                                            src="/img/decoration/epic-shadown.png"
                                            className="epic-shadown"
                                        />
                                    </div>
                                </Col>
                                <Col lg={5}>
                                    <div className="epic-infor">
                                        <div className="epic-title">
                                            {epic.title}
                                        </div>
                                        <img
                                            src="/img/decoration/epic-line.png"
                                            className="epic-line"
                                        />
                                        <div className="epic-desc">
                                            {epic.desc}
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </div>

                        <div className="epic-slide">
                            <Row className="justify-content-center">
                                <Col lg={9}>
                                    <div className="wrap-slide-epic">
                                        <img
                                            src="/img/icons/arrow-left.png"
                                            className="epic-arrow epic-prev"
                                        />
                                        <Swiper
                                            spaceBetween={15}
                                            slidesPerView={5}
                                            navigation={{
                                                nextEl: '.epic-next',
                                                prevEl: '.epic-prev',
                                            }}
                                            ref={sliderEpicref}
                                        >
                                            {epicData.map((epic, index) => (
                                                <SwiperSlide
                                                    key={index}
                                                    onClick={() => {
                                                        if (
                                                            epic.type ===
                                                            'normal'
                                                        ) {
                                                            setEpic(epic);
                                                        }
                                                    }}
                                                >
                                                    <EpicSlideItem
                                                        epic={epic}
                                                        index={index}
                                                        sliderEpicRef={
                                                            sliderEpicref
                                                        }
                                                    ></EpicSlideItem>
                                                </SwiperSlide>
                                            ))}
                                        </Swiper>
                                        <img
                                            src="/img/icons/arrow-right.png"
                                            className="epic-arrow epic-next"
                                        />
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
            <img
                src="/img/decoration/bg2.c8819f39.png"
                className="light-box light-box-epic-bottom"
            />
        </div>
    );
}
