import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import titleIconData from '../../public/data/iconTitle';

export default function Download() {
    const percentage = 70;

    return (
        <div className="block block-ecosystem">
            <Container>
                <div className="title-img text-center">
                    {titleIconData.map((item, index) => (
                        <img src={item.image} />
                    ))}
                </div>
                <div className="block-title text-center wow fadeInUp  animated">
                    <p>Free To Play, Play to Earn  </p>
                    <p className="block-subtitle">The ABI Ecosystem</p>
                </div>
                <div className="block-content">
                    <Row className="justify-content-center wow fadeInUp  animated">
                        <Col md={3} sm={3}>
                            <div className="list-item-tab">
                                <ul className="list-item text-center">
                                    <li className="item"><a className="btn btn-item">About ABI</a></li>
                                    <li className="item"><a className="btn btn-item">Buyback and Burn</a></li>
                                    <li className="item"><a className="btn btn-item">VeABI</a></li>
                                    <li className="item"><a className="btn btn-item">ABI Creator</a></li>
                                    <li className="item"><a className="btn btn-item">ROAD MAP</a></li>
                                </ul>
                            </div>
                        </Col>
                        <Col md={9} sm={9}>
                            <div className="content text-center">
                                <div className="circular text-center">
                                    <div style={{ width: 250, height: 250 }}>
                                    <CircularProgressbar value={70} />
                                    </div>
                                    <div className="youtube"><img src="/images/youtube.png" /></div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-7 col-lg-12">
                                        <div className="progress">
                                            <div className="progress-bar"></div>
                                        </div>
                                    </div>
                                    <div className="col-xl-5 col-lg-12">
                                        <div className="control text-xl-right">
                                            <a href="#"><img src="/images/pre.svg" /></a>
                                            <a href="#"><img src="/images/pause.svg" /></a>
                                            <a href="#"><img src="/images/next.svg" /></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
