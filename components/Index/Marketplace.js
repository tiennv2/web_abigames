import React from 'react';
import Slider from "react-slick";
import { Container, Row, Col } from 'react-bootstrap';
import titleIconData from '/public/data/iconTitle';
import dataMarketplace from '/public/data/marketplace';

export default function Maketplace() {

    var settings = {
        speed: 500,
        slidesToShow: 6,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 993,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                }
            },
          ]
    };

    return (
        <div className="block block-marketplace">
            <Container>
                <div className="block-header">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>An Amazing App Can Change Your Daily Life</p>
                        <p className="block-subtitle">GAR Experience</p>
                    </div>
                </div>
                <div className="block-content text-center">
                <Slider {...settings}>
                    {dataMarketplace.map((item, index) => (
                        <div className="block-intro-item">
                            <div className="img"><img src={item.image} /></div>
                            <div className="content text-center">
                                <div className="item-title">
                                    <p>{item.title} </p>
                                    <p>{item.subtitle}</p>
                                </div>
                                <div className="price">{item.note} <span><img src={item.icon}/></span> {item.price}</div>
                            </div>
                        </div>
                    ))}
                </Slider>
                </div>
                <div className="block-inrto-nft">
                    <div className="ntf-logo d-none d-sm-block"><img src="./images/nft_logo.svg" /></div>
                    <div className="intro-nft-content text-center">
                        <div className="tille">NFT MAKETCPLACE &amp; CoinMarketCap Abi</div>
                        <div className="text">Meet ABI BOX to C.M.C Datatrom , Watch it as it nuts through data,<br/>  laborates statistics &amp; is up for for any challenge &amp; ready to take on any of  the <br/>ABI in the ABI BOX .</div>
                    </div>
                </div>
            </Container>
        </div>
    );
}
