import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import downloadData from '../../public/data/download';
import titleIconData from '../../public/data/iconTitle';

export default function Download() {
    return (
        <div className="block block-download">
            <Container>
                <div className="title-img text-center">
                    {titleIconData.map((item, index) => (
                        <img src={item.image} />
                    ))}
                </div>
                <div className="block-title text-center wow fadeInUp  animated">
                    <p>Play anywhere, anytime  </p>
                    <p className="block-subtitle">The MOBOX experience on any device.</p>
                    <p>(iOS, Android, Desktop)</p>
                </div>
                <div className="block-content">
                    <Row className="justify-content-center wow fadeInUp  animated">
                        <Col lg={6}>
                            <Row>
                                {downloadData.map((download, index) => (
                                    <div  key={index} className="col-12 col-sm-6">
                                        <div className="img-app"><a href={download.link} ><img src={download.icon} title={download.title} /></a></div>
                                    </div>
                                ))}
                            </Row>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
