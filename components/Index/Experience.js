import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import titleIconData from '/public/data/iconTitle';
import experienceData from '/public/data/experience';

export default function News() {
    return (
        <div className="block block-experience">
            <Container>
                <div className="block-header">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>An Amazing App Can Change Your Daily Life</p>
                        <p className="block-subtitle">GAR Experience</p>
                    </div>
                </div>
                <div className="block-content">
                    <Row>
                        {experienceData.map((item, index) => (
                            <Col lg={4} md={4} xl={4}>
                                <div className="block-experience-item wow fadeInUp animated">
                                    <div className="item-img">
                                        <img src={item.image} />
                                    </div>
                                    <div className="content">
                                        <div className="item-title float-left">
                                            <p>{item.title}</p>
                                            <p className="sub-title">{item.desc}</p>
                                        </div>
                                        <a href="#" className="btn read-more float-right">READ MORE</a>
                                    </div>
                                </div>
                            </Col>
                        ))}
                    </Row>
                </div>
            </Container>
        </div>
    );
}
