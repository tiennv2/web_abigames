import React, { Component } from 'react';
import { Container, Row, Form, Col } from 'react-bootstrap';
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import footerData from '/public/data/footer';
import titleIconData from '/public/data/iconTitle';

export default function Footer() {
    return (
        <div className="block block-resources">
            <Container>
                <Row>
                    <Col xl={6}>
                        <div className="resources-left">
                            <Row>
                                {footerData.map((item, index) => (
                                    <Col className={item.class}>
                                        <div className="item text-center"><img src={item.image} /></div>
                                    </Col>
                                ))}
                            </Row>
                        </div>
                    </Col>
                    <Col xl={6}>
                        <div className="resources-right">
                            <div className="form-search">
                                <Form className="form" >
                                    <div className="input-group">
                                        <div className="form-outline">
                                            <input id="search-input" type="search" id="form" className="form-control" placeholder="Enter keyword or URL" value="" required="" />
                                        </div>
                                        <button id="search-button" type="submit" className="btn btn-primary">
                                            <FontAwesomeIcon icon={faSearch} />
                                        </button>
                                    </div>
                                </Form>
                            </div>
                            <div className="title-img text-center">
                                {titleIconData.map((item, index) => (
                                    <img src={item.image} />
                                ))}
                            </div>
                            <div className="block-title">
                                <p>Free To Play, Play to Earn  </p>
                                <p className="block-subtitle">Resources For Getting Started</p>
                                <p className="text">Duis feugiat congue metus, ultrices vulputate nibh viverra eget. Vestibulum ullamcorper volutpat arius. </p>
                            </div>
                            <div className="image"><img src="/images/footer-5.svg" /></div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
