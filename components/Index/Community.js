import React, { useState } from 'react';
import { Container, Tab, Nav, Row, Col } from 'react-bootstrap';
import communitys from '../../public/data/communitys';
import titleIconData from '../../public/data/iconTitle';

export default function Community() {
    const [community, setcommunity] = useState(communitys[0]);

    return (
        <div className="block block-community">
            <span className="background"></span>
            <Container>
                <div className="title-img text-center">
                    {titleIconData.map((item, index) => (
                        <img src={item.image} />
                    ))}
                </div>
                <div className="block-title text-center wow fadeInUp  animated">
                    <p>Free To Play, Play to Earn  </p>
                    <p className="block-subtitle">THIS IS OUR COMMUNITY!</p>
                </div>
                <Row>
                    <Col md={3}>
                        <div className="img">
                            <img src="./images/tele-group.svg" />
                        </div>
                    </Col>
                    <Col md={9} className="text-center wow fadeInUp  animated">
                        <div className="title">Telegram <span>80K+</span></div>
                        <div className="text">Communicate on different themes, share your game successes, find friends and like-minded people! Join our chat now!</div>
                        <div className="btn">Subscribe</div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
