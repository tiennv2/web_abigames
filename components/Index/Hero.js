import React, { useState, useRef, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import spineWidget from '../../public/js/spine-widget';

import moboData from '../../public/data/mobo';
const moboEvutionArr = [...Array(4)];

const MoboItemSlide = ({ mobo, moboSlideRef, index }) => {
    const mobiItemSlideRef = useRef(null);

    const resetMoboSlide = () => {
        moboSlideRef.current
            .querySelectorAll('.mobo-item-slide')
            .forEach((element) => {
                element.classList.remove('active');
            });
    };

    return (
        <div
            ref={mobiItemSlideRef}
            className={
                index === 0 ? 'mobo-item-slide active' : 'mobo-item-slide'
            }
            onClick={() => {
                resetMoboSlide();
                mobiItemSlideRef.current.classList.add('active');
            }}
        >
            <img
                src="/img/decoration/light.145ea0a7.png"
                className="lightMobo"
            />
            <img
                className="moboAva"
                src={`/img/mobo/${mobo.moboId}/evution/momo_${mobo.moboId}_1.png`}
            />
        </div>
    );
};

const MoboEvution = ({ mobo, index }) => (
    <img
        src={`/img/mobo/${mobo.moboId}/evution/momo_${mobo.moboId}_${index}.png`}
    />
);

export default function Hero() {
    const moboSlideRef = useRef(null);
    const heroSpineRef = useRef(null);

    const [mobo, setMobo] = useState(moboData[0]);

    useEffect(() => {
        spineWidget(mobo.moboId, heroSpineRef.current);
    });

    return (
        <div className="block block-hero">
            <div className="block-wrapper">
                <img
                    src="/img/decoration/bg2.c8819f39.png"
                    className="light-box light-box-hero"
                />
                <Container>
                    <div className="block-header">
                        <div className="block-header-left">
                            <div className="slogan">
                                <img src="/img/decoration/slogan.7452dbef.png" />
                            </div>
                        </div>
                        <div className="block-header-right">
                            <div className="block-header-meta">
                                <div className="block-title">Gaming NFTs</div>
                                <div className="block-subtitle">
                                    FREE TO PLAY, PLAY TO EARN
                                </div>
                            </div>
                            <div className="border-title"></div>
                        </div>
                    </div>
                    <div className="block-content">
                        <div className="mobo-meta">
                            <div className="mobo-meta-left">
                                <div className="mobo-spine">
                                    <div className="hero-wrap">
                                        <canvas
                                            id="hero-spine"
                                            ref={heroSpineRef}
                                        ></canvas>
                                    </div>
                                </div>
                            </div>
                            <div className="mobo-meta-right">
                                <div className="mobo-evution-line">
                                    <div className="mobo-evution">
                                        {moboEvutionArr.map((number, index) => (
                                            <MoboEvution
                                                key={index}
                                                mobo={mobo}
                                                index={index + 1}
                                            ></MoboEvution>
                                        ))}
                                    </div>
                                </div>
                                <div className="mobo-line-name">
                                    <img src="/img/icons/icon.webp" />
                                    <span className="name-first">
                                        {mobo.nameFirst}
                                    </span>
                                    <span className="name-last">
                                        {mobo.nameLast}
                                    </span>
                                </div>
                                <div className="mobo-sub">{mobo.moboDesc}</div>
                            </div>
                        </div>
                        <div className="mobo-slide">
                            <Row className="justify-content-center">
                                <Col lg={11} className="p-0">
                                    <div className="wrap-slide-mobo">
                                        <img
                                            src="/img/decoration/ourgame-left.png"
                                            className="ourgame-arrow mobo-left"
                                        />
                                        <Swiper
                                            ref={moboSlideRef}
                                            spaceBetween={16}
                                            slidesPerView={14}
                                            navigation={{
                                                nextEl: '.mobo-right',
                                                prevEl: '.mobo-left',
                                            }}
                                        >
                                            {moboData.map((mobo, index) => (
                                                <SwiperSlide
                                                    key={index}
                                                    onClick={() => {
                                                        setMobo(mobo);
                                                    }}
                                                >
                                                    <MoboItemSlide
                                                        index={index}
                                                        mobo={mobo}
                                                        moboSlideRef={
                                                            moboSlideRef
                                                        }
                                                    ></MoboItemSlide>
                                                </SwiperSlide>
                                            ))}
                                        </Swiper>
                                        <img
                                            src="/img/decoration/ourgame-left.png"
                                            className="ourgame-arrow mobo-right"
                                        />
                                    </div>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
        </div>
    );
}
