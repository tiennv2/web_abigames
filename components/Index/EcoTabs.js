import React from 'react';
import { Container, Tab, Row, Col, Nav } from 'react-bootstrap';
import { ecoTabData } from '../../public/data/ecoTab';
import AboutTab from './EcoTab/AboutTab';
import BuyBackTab from './EcoTab/BuyBackTab';
import CreatorTab from './EcoTab/CreatorTab';
import RoadMapTab from './EcoTab/RoadMapTab';
import VeMbox from './EcoTab/VeMbox';

export default function Eco() {
    return (
        <div className="block block-eco">
            <div className="block-wrapper">
                <img
                    src='/img/decoration/bg2.c8819f39.png'
                    className="light-box light-box-epic-top"
                />
                <Container>
                    <div className="block-header">
                        <div className="block-header-right">
                            <div className="block-header-meta">
                                <div className="block-title">
                                    ABI Platform Ecosystem
                                </div>
                                <div className="block-subtitle">
                                    FREE TO PLAY, PLAY TO EARN
                                </div>
                            </div>
                            <div className="border-title"></div>
                        </div>
                    </div>
                    <div className="block-content">
                        <div className="eco-tab">
                            <Tab.Container
                                id="eco-tab-list"
                                defaultActiveKey="roadmap"
                            >
                                <Row>
                                    <Col lg={3}>
                                        <Nav
                                            variant="pills"
                                            className="flex-column eco-nav"
                                        >
                                            {ecoTabData.map((tab, index) => (
                                                <Nav.Item key={index}>
                                                    <Nav.Link eventKey={tab.id}>
                                                        {tab.title}
                                                    </Nav.Link>
                                                </Nav.Item>
                                            ))}
                                        </Nav>
                                    </Col>
                                    <Col lg={9}>
                                        <Tab.Content>
                                        <Tab.Pane eventKey="roadmap">
                                                <RoadMapTab></RoadMapTab>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="about">
                                                <AboutTab></AboutTab>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="creator">
                                                <CreatorTab></CreatorTab>
                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Row>
                            </Tab.Container>
                        </div>
                    </div>
                </Container>
            </div>
            <img
                src='/img/decoration/bg2.c8819f39.png'
                className="light-box light-box-epic-bottom"
            />
        </div>
    );
}
