import React, { useState } from 'react';
import { Tab, Row, Col, Nav } from 'react-bootstrap';
import aboutTabData from '../../../public/data/aboutTab';

export default function AboutTab() {
    const [video, setVideo] = useState(aboutTabData[0].videoUrl);

    return (
        <div className="eco-tab-content about-tab-content">
            <Tab.Container id="about-tab" defaultActiveKey="first">
                <Row>
                    <Col lg={12}>
                        <div className="video-iframe">
                            <iframe
                                width="100%"
                                height="100%"
                                src={video}
                                title=""
                                frameBorder={0}
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                            ></iframe>
                        </div>
                    </Col>
                </Row>
            </Tab.Container>
        </div>
    );
}
