import React from 'react';
import { Row, Col } from 'react-bootstrap';

export default function BuyBackTab() {
    return (
        <div className="eco-tab-content buyback-tab-content">
            <Row>
                <Col lg={6}>
                    <div className="title-box">
                        <div className="title-box-name">Buyback Pool</div>
                        <div className="title-box-value">$5.454.035</div>
                    </div>
                </Col>
                <Col lg={6}>
                    <div className="title-box">
                        <div className="title-box-name">Total Burn</div>
                        <div className="title-box-value">MBOX 7.149.692</div>
                    </div>
                </Col>
                <Col>
                    <div className="tab-desc">
                        <div className="process-img">
                            <img src="/img/tab/process.png" />
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    );
}
