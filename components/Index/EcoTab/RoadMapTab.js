import React from 'react';
import rmData from '../../../public/data/roadMap';

export default function RoadMapTab() {
    return (
        <div className="eco-tab-content roadmap-tab-content">
            <div className="roadmap-content">
                {rmData.map((stage, index) => (
                    <div key={index} className="stage">
                        <div className="quater">
                            <div className="quater-icon">
                                <img src="/img/icons/time.png" />
                            </div>
                            <div className="quater-tile">{stage.quater}</div>
                        </div>
                        <div className="schedule">
                            <div className="schedule-line">
                                {stage.tasks.map((task, index) => (
                                    <div
                                        key={index}
                                        className={
                                            task.isComplete
                                                ? 'task-item complete'
                                                : 'task-item'
                                        }
                                    >
                                        {task.title}
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="stage-status">{stage.status}</div>
                    </div>
                ))}
            </div>
        </div>
    );
}
