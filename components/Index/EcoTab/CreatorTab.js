import React from 'react';

export default function CreatorTab() {
    return (
        <div className="eco-tab-content creator-tab-content">
            <div className="creator-pane">
                <canvas id="creator-board"></canvas>
                <div className="creator-logo">
                    <img src="/img/GAR.svg" />
                    <div className="creator-logo-meta">
                        <div className="creator-logo-title">MAKE ME YOURS</div>
                        <div className="creator-logo-desc">COMING SOON</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
