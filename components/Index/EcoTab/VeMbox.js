import React from 'react';
import { Row, Col } from 'react-bootstrap';

export default function VeMbox() {
    return (
        <div className="eco-tab-content vembox-tab-content">
            <Row>
                <Col lg={6}>
                    <div className="title-box">
                        <div className="title-box-name">Total MBOX Staked</div>
                        <div className="title-box-value">10.539.597</div>
                    </div>
                </Col>
                <Col lg={6}>
                    <div className="title-box">
                        <div className="title-box-name">Total veMBOX</div>
                        <div className="title-box-value">126.140.160</div>
                    </div>
                </Col>
                <Col lg={12}>
                    <div className="title-box">
                        <div className="title-box-name">
                            Average Stake Time(D)
                        </div>
                        <div className="title-box-value">280</div>
                    </div>
                </Col>
                <Col>
                    <div className="tab-desc">
                        ·veMBOX will be used as the only voting token for
                        important decision on the MOBOX platform. veMBOX can
                        only be obtained through staking MBOX.
                        <br />
                        <br />
                        ·veMBOX cannot be traded.
                        <br />
                        <br />
                        ·veMBOX also gives a bonus weight boost for Crates MBOX
                        mining, up to 3X (use the calculator to estimate how
                        much veMBOX is needed).
                    </div>
                </Col>
            </Row>
        </div>
    );
}
