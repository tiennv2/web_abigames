import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import visionData from '/public/data/vision'
import titleIconData from '/public/data/iconTitle';;

export default function News() {
    return (
        <div className="block block-vision">
            <Container>
                <Row>
                    <Col lg={6}>
                        <Row>
                            {visionData.map((item, index) => (
                                <Col className={item.class}>
                                    <div className="item text-center">
                                        <div className="img"><a href={item.link}><img src={item.icon}></img></a></div>
                                        <div className="note"><a href={item.link}>{item.title}</a></div>
                                    </div>
                                </Col>
                            ))}
                        </Row>
                    </Col>
                    <Col lg={6}>
                        <div className="block-header">
                            <div className="title-img text-center">
                                {titleIconData.map((item, index) => (
                                    <img src={item.image} />
                                ))}
                            </div>
                            <div className="block-title text-center wow fadeInUp  animated">
                                <p>Free To Play, Play to Earn  </p>
                                <p className="block-subtitle">Our Vision</p>
                            </div>
                            <div className="text wow fadeInUp  animated">Our vision is that no NFT metaverse should be singular but rather each metaverse can be interconnected giving each unique NFT increased utility through NFT interoperability between games and platforms.</div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
