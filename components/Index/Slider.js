import React from 'react';
import dataSlider from '/public/data/slider';

export default function Slide() {
    return (
        <div className="block block-slider">
            <div className="banner-img">
                {dataSlider.map((item, index) => (
                    <img src={item.image} className={item.class} />
                ))}
            </div>
            <div className="content text-center wow fadeInUp  animated">
                <h4 className="note">FREE TO PLAY</h4>
                <h3 className="title">PLAY TO EARN</h3>
                <p className="text">The First Blockchain Galaxy Attack<br />Alien Shooter Game</p>
                <p className="text">on</p>
                <p className="image text-center">
                    <img src="/images/App Store.svg" />
                    <img src="/images/Google Play.svg" />
                </p>
            </div>
        </div>
    );
}
