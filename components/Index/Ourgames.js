import React from 'react';
import Slider from "react-slick";
import { Container, Row, Col } from 'react-bootstrap';
import titleIconData from '/public/data/iconTitle';
import dataNft from '/public/data/nftitem';

export default function News() {

    var settings = {
        speed: 500,
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4,
                }
            },
            {
                breakpoint: 993,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 2,
                }
            },
          ]
    };

    return (
        <div className="block block-gaming">
            <Container>
                <div className="block-header">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>Free To Play, Play to Earn</p>
                        <p className="block-subtitle">Our Gaming NFTs</p>
                    </div>
                </div>
                <div className="block-content text-center nft-item">
                    <Row>
                        <Col xl={3}>
                            <div className="image text-center">
                                <a href="#"><img src="/images/NFT.svg" /></a>
                            </div>
                        </Col>
                        <Col xl={9}>
                            <div className="nft-slider">
                                <Slider {...settings}>
                                    {dataNft.map((item, index) => (
                                        <div className="item text-center">
                                            <a href="#"><img src={item.image} /></a>
                                            
                                        </div>
                                    ))}
                                </Slider>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className="nft-abi">
                    <Row>
                        <Col xl={4} lg={12}>
                            <div className="title text-center">Own Your First NFT ABI</div>
                        </Col>
                        <Col xl={8} lg={12}>
                            <div className="buy-item">
                                <Row>
                                    <Col md={4}>
                                        <div className="item">
                                            <a href="#" className="btn">SpaceShip</a>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div className="item">
                                            <a href="#" className="btn">Buy ABI </a>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div className="item">
                                            <a href="#" className="btn">Buy SpaceShip</a>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
