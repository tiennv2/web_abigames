import React, { useState, useRef, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';
import shipWidget from '../../public/js/spine-ship';

import shipData from '../../public/data/ship';
const shipEvutionArr = [...Array(4)];

const ShipItemSlide = ({ ship, shipSlideRef, index }) => {

    const mobiItemSlideRef = useRef(null);

    const resetShipSlide = () => {
        shipSlideRef.current.querySelectorAll('.item-slide').forEach((element) => {
            element.classList.remove('active');
        });
    };

    return (
        <div
            ref={mobiItemSlideRef}
            className={ index === 0 ? 'item-slide active' : 'item-slide' }
            onClick={() => { resetShipSlide(); mobiItemSlideRef.current.classList.add('active'); }}
        >
            <img src="/img/decoration/light.145ea0a7.png" className="light" />
            <img className="Ava" src={`/images/ship/evution/ship${ship.shipId}.png`} />
        </div>
    );
};

const ShipEvution = ({ ship, index }) => (
    <img src={`/images/ship/evution/ship${ship.shipId}.png`} />
);

export default function ShipDetail() {
    const shipSlideRef = useRef(null);
    const shipSpineRef = useRef(null);

    const [ship, setShip] = useState(shipData[0]);
    useEffect(() => {
        shipWidget(ship.shipId, shipSpineRef.current);
    });

    return (
        <div className="block block-hero">
            <div className="block-wrapper">
                <img src="/img/decoration/bg2.c8819f39.png" className="light-box light-box-hero" />
                <Container>
                    <div className="block-header"></div>
                    <div className="block-content">
                        <div className="meta">
                            <div className="meta-left">
                                <div className="spine">
                                    <div className="hero-wrap">
                                        <canvas id="hero-spine" ref={shipSpineRef} ></canvas>
                                    </div>
                                </div>
                            </div>
                            <div className="meta-right">
                                <div className="line-name">
                                    <img src="/img/icons/icon.webp" />
                                    <span className="name-first"> {ship.nameFirst} </span>
                                    <span className="name-last"> {ship.nameLast} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img src="/img/decoration/ourgame-left.png"className="ourgame-arrow left" />
                    <Swiper
                        ref={shipSlideRef}
                        spaceBetween={16}
                        slidesPerView={10}
                        navigation={{
                            nextEl: '.ship-right',
                            prevEl: '.ship-left',
                        }}
                    >
                        {shipData.map((ship, index) => (
                            <SwiperSlide key={index} onClick={() => { setShip(ship); }} >
                                <ShipItemSlide
                                    index={index} ship={ship} shipSlideRef={ shipSlideRef }
                                ></ShipItemSlide>
                            </SwiperSlide>
                        ))}
                    </Swiper>
                    <img src="/img/decoration/ourgame-left.png" className="ourgame-arrow right" />
                </Container>
            </div>
        </div>
    );
}
