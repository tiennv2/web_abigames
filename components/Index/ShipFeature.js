import React from 'react';
import { Container } from 'react-bootstrap';
import ShipList from '../ArchiveShip/ShipList';
import shipData from '../../public/data/ships';

export default function ShipFeature() {
    return (
        <div className="block block-ship-feature">
            <div className="block-wrapper">
                <Container>
                    <div className="block-header block-header-start">
                        <div className="block-header-right">
                            <div className="border-title"></div>
                            <div className="block-header-meta">
                                <div className="block-title">
                                    SPACE SHIPs Feature
                                </div>
                                <div className="block-subtitle">
                                    FREE TO PLAY, PLAY TO EARN
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="block-content">
                        <ShipList col={2} dataListShip={shipData}></ShipList>
                    </div>
                </Container>
            </div>

        </div>
    );
}
