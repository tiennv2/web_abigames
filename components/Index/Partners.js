import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import partnersData from '../../public/data/partners';
import titleIconData from '../../public/data/iconTitle';

export default function Partners() {
    return (
        <div className="block block-partners">
            <div className="block-wrapper">
                <Container>
                    <div className="block-header">
                        <div className="title-img text-center">
                            {titleIconData.map((item, index) => (
                                <img src={item.image} />
                            ))}
                        </div>
                        <div className="block-title text-center wow fadeInUp  animated">
                            <p>Free To Play, Play to Earn  </p>
                            <p className="block-subtitle">Strategic Partners</p>
                        </div>
                    </div>
                    <div className="block-content">
                        <div className="partner-list">
                            <Row>
                                {partnersData.map((partner, index) => (
                                    <Col className={partner.class} key={index}>
                                        <a
                                            href={partner.link}
                                            className="partner-item"
                                        >
                                            <img src={partner.image} />
                                        </a>
                                    </Col>
                                ))}
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
        </div>
    );
}
