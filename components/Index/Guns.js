import React, { useState, useRef, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Swiper, SwiperSlide } from 'swiper/react';

import gunData from '../../public/data/gunData';

const GunItemSlide = ({ gun, gunSlideRef, index }) => {
    console.log(gun);
    const mobiItemSlideRef = useRef(null);

    const resetgunSlide = () => {
        gunSlideRef.current.querySelectorAll('.mobo-item-slide').forEach((element) => {
            element.classList.remove('active');
        });
    };

    return (
        <div
            ref={mobiItemSlideRef}
            className={
                index === 0 ? 'item-slide active' : 'item-slide'
            }
            onClick={() => {
                resetgunSlide();
                mobiItemSlideRef.current.classList.add('active');
            }}
        >
            <img
                src="/img/decoration/light.145ea0a7.png"
                className="lightMobo"
            />
            <img
                className="moboAva"
                src={`/images/gun/evution/gun${gun.gunId}.png`}
            />
        </div>
    );
};

const gunEvution = ({ gun, index }) => (
    <img
        src={`/images/gun/evution/gun${gun.gunId}.png`}
    />
);

export default function Hero() {
    const gunSlideRef = useRef(null);
    const gunSpineRef = useRef(null);

    const [gun, setgun] = useState(gunData[0]);

    return (
        <div className="block block-hero">
            <div className="block-wrapper">
                <Container>
                    <div className="block-content">
                        <div className="mobo-meta">
                            <div className="mobo-meta-left">
                                <div className="mobo-spine">
                                    <div className="hero-wrap">
                                        <canvas
                                            id="hero-spine"
                                            ref={gunSpineRef}
                                        ></canvas>
                                    </div>
                                </div>
                            </div>
                            <div className="mobo-meta-right">
                                <div className="mobo-line-name">
                                    <span className="name-first">
                                        {gun.name}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img
                            src="/img/decoration/ourgame-left.png"
                            className="ourgame-arrow mobo-left"
                        />
                        <Swiper
                            ref={gunSlideRef}
                            spaceBetween={16}
                            slidesPerView={10}
                            navigation={{
                                nextEl: '.gun-right',
                                prevEl: '.gun-left',
                            }}
                        >
                            {gunData.map((gun, index) => (
                                <SwiperSlide
                                    key={index}
                                    onClick={() => {
                                        setgun(gun);
                                    }}
                                >
                                    <GunItemSlide
                                        index={index}
                                        gun={gun}
                                        gunSlideRef={ gunSlideRef }
                                    ></GunItemSlide>
                                </SwiperSlide>
                            ))}
                        </Swiper>
                        <img
                            src="/img/decoration/ourgame-left.png"
                            className="ourgame-arrow mobo-right"
                        />
                </Container>
            </div>
        </div>
    );
}
