import React from 'react';
import { Container } from 'react-bootstrap';
import adverData from '../../public/data/adver';
import titleIconData from '../../public/data/iconTitle';

export default function Adver() {
    return (
        <div className="block block-adver">
            <Container>
                <div className="block-header block-header-start">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>Free To Play, Play to Earn  </p>
                        <p className="block-subtitle">Milestones</p>
                    </div>
                </div>
                <div className="block-content d-flex justify-content-between bd-highlight mb-3">
                    {adverData.map((item, index) => (
                        <div className="p-2 bd-highlight text-center block-item" key={index}>
                            <div className="adver-content">
                                <div className="image">
                                    <img src={item.image} />
                                </div>
                                <div className="content">
                                    <div className="title">{item.title}</div>
                                    <div className="text">{item.desc}</div>
                                    <a href="#" className="btn">{item.name}</a>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </Container>
        </div>
    );
}
