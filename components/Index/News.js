import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import newsData from '/public/data/news'
import titleIconData from '/public/data/iconTitle';;

export default function News() {
    return (
        <div className="block block-news">
            <Container>
                <div className="block-header">
                    <div className="title-img text-center">
                        {titleIconData.map((item, index) => (
                            <img src={item.image} />
                        ))}
                    </div>
                    <div className="block-title text-center wow fadeInUp  animated">
                        <p>Free To Play, Play to Earn  </p>
                        <p className="block-subtitle">Latest News</p>
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        <div className="className"><img src="/images/Rectangle_647.png" /></div>
                    </Col>
                    <Col lg={8}>
                        <div className="block-content">
                            {newsData.map((news, index) => (
                                <div className="item-content wow fadeInUp  animated" key={index}>
                                    <a href={news.link} className="news-item">
                                        <div className="news-count number">{`0${index + 1}`}</div>
                                        <div className="detail">
                                            <div className="title">{news.title}</div>
                                            <div className="console">{news.desc}</div>
                                        </div>
                                    </a>
                                </div>
                            ))}
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
