import Slider from '../components/Index/Slider';
import Experience from '../components/Index/Experience';
import Marketplace from '../components/Index/Marketplace';
import ShipDetail from '../components/Index/ShipDetail';
import Adver from '../components/Index/Adver';
import Ourgames from '../components/Index/Ourgames';
import Recources from '../components/Index/Recources';
import Ecosystem from '../components/Index/Ecosystem';
import Vision from '../components/Index/Vision';
import Gallery from '../components/Index/Gallery';
import Community from '../components/Index/Community';
import News from '../components/Index/News';
import Download from '../components/Index/Download';
import Guns from '../components/Index/Guns';
import Hero from '@/components/Index/Hero';

export default function Home() {
    return (
        <div className="home-content">
            <Slider></Slider>
            <Experience></Experience>
            <Marketplace></Marketplace>
            <ShipDetail></ShipDetail>
            <Guns></Guns>
            <Adver></Adver>
            <Ourgames></Ourgames>
            <Recources></Recources>
            <Ecosystem></Ecosystem>
            <Vision></Vision>
            <Gallery></Gallery>
            <Community></Community>
            <News></News>
            <Download></Download>
        </div>
    );
}
