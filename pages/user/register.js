import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import AuthContext from '@/context/AuthContext';
import Link from 'next/link';
import axios from 'axios';

export default function RegisterPage() {
    const [email, setEmail] = useState("");
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const {register, error} = useContext(AuthContext);
    const handleSubmit = (event) => {
        event.preventDefault();
        if(password !== confirmPassword){
            toast.error("Password do not match!");
            console.log(toast.error);
            return;
        }
        register({ username, email, password });
    };
    return (
        <div className=" block block-login-form">
            <Container>
                <Row className="justify-content-center align-items-center">
                    <Col md={6} className="login-box">
                        <Form onSubmit={handleSubmit} id="login-form" className="form" action="" method="post">
                            <h3 className="text-center text-info">Register</h3>
                            <div className="form-group">
                                <label htmlFor="username" className="text-info">Username:</label>
                                <input type="username" name="username" id="username" className="form-control" placeholder="Username" required value={username} onChange={(e) => setUserName(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email" className="text-info">Email:</label>
                                <input type="email" name="identifier" id="identifier" className="form-control" placeholder="Email" required value={email} onChange={(e) => setEmail(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" className="text-info">Password:</label>
                                <input type="password" name="password" id="password" className="form-control" placeholder="Password" required value={password} onChange={(e) => setPassword(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" className="text-info">Confirm Password:</label>
                                <input type="password" name="confirmPassword" id="confirmPassword" className="form-control" placeholder="Confirm Password" required value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
                            </div>
                            <div className="form-group">
                                <input type="submit" name="submit" className="btn btn-info btn-md" value="submit" />
                            </div>
                            <div id="register-link" className="text-right register-link">
                                <a href="/user/login" className="text-info">Login here</a>
                            </div>                            
                        </Form>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
