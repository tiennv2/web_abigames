import { useContext, useState, useEffect } from 'react';
import AuthContext from '@/context/AuthContext';
import { Container, Row, Col, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Link from 'next/link';

export default function LoginPage() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const {login, error} = useContext(AuthContext);

    useEffect(() => error && toast.error(error));

    const handleSubmit = (event) => {
        event.prevenDefault();
        login({email, password});
    };

    return (
        <div className="block block-login-form">
            <Container>
                <Row className="justify-content-center align-items-center">
                    <Col md={6} className="login-box">
                        <form onSubmit={handleSubmit} className="form" method="POST">
                            <h3 className="text-center text-info">Login</h3>
                            <div className="form-group">
                                <label htmlFor="username" className="text-info">Email:</label>
                                <input type="email" name="identifier" id="identifier" className="form-control" placeholder="Email" required value={email} onChange={(event) => setEmail(event.target.value)} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="password" className="text-info">Password:</label>
                                <input type="password" name="password" id="password" className="form-control" placeholder="Password" required value={password} onChange={(event) => setPassword(event.target.value)} />
                            </div>
                            <div className="form-group">
                                <label htmlFor="remember-me" className="text-info">
                                    <span>Remember me</span> 
                                    <span><input id="remember-me" name="remember-me" type="checkbox" /></span>
                                </label>
                            </div>
                            <div className="form-group">
                                <input type="submit" className="btn btn-info btn-md" value="submit" />
                            </div>
                            <div id="register-link" className="text-right register-link">
                                <a href="/user/register" className="text-info">Register here</a>
                            </div>                            
                        </form>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
