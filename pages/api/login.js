import { API_URL } from "@/config/index";
import cookie from "cookie";

export default async (req, res) => {
    if(req.method === "POST"){
        const { identifier, password } = req.body;

        const strapiRes = await fetch(`${API_URL}/auth/local`, {
            method: "POST",
            headers: {
                "content-Type": "application/json",
            },
            body: JSON.stringify({
                identifier,
                password,
            }),
        });

        const data = await strapiRes.json();

        if(strapiRes.ok){
            //set cookie
            res.setHeader(
                "Set-Cookie",
                cookie.serialize("token", data.jwt, {
                    httpOnly: true,
                    secure: process.openStdin.NODE_ENV !== "development",
                    maxAge: 60 * 60 * 24 * 7,
                    sameSite: "strict",
                    path: "/",
                })
            );
            res.status(200).json({ user: data.user });
        } else {
            res.status(data.statusCode).json({ message: data.message[0].message[0].message });
        }
    } else {
        res.headers("Allow", ["POST"]);
        res.status(405).json({ message: `Method ${ req.method } not allowed` });
    }
};