import React from 'react';
import { Col, Container, Row, Form } from 'react-bootstrap';
import FilterCheckBoxItem from '../../components/ArchiveShip/FilterCheckBoxItem';
import ShipList from '../../components/ArchiveShip/ShipList';
import shipData from '../../public/data/ships';

export default function ShipArchiveList() {
    return (
        <div className="block block-archive-ship">
            <div
                className="page-banner"
                style={{
                    backgroundImage: `url('/img/decoration/galaxy-banner.jpg')`,
                }}
            >
                <div className="banner-content">
                    <div className="banner-titles">
                        <div className="banner-title">THE TRADING PORT</div>
                        <div className="banner-subtitle">Space Ship</div>
                    </div>
                    <img
                        src="/img/decoration/banner-title-bg.png"
                        className="banner-title-bg"
                    />
                </div>
                <img
                    src="/img/decoration/banner-bar.png"
                    className="banner-bar"
                />
            </div>
            <Container fluid>
                <Row className="justify-content-center">
                    <Col lg={10}>
                        <div className="archive-summary">
                            <div className="summary-box">
                                <div className="summary-title">Ships Sold</div>
                                <div className="summary-value">10,146</div>
                            </div>
                            <div className="summary-box">
                                <div className="summary-title">
                                    Total volume
                                </div>
                                <div className="summary-value">18,496 BNB</div>
                            </div>
                            <div className="summary-box">
                                <div className="summary-title">
                                    Highest price
                                </div>
                                <div className="summary-value">200 BNB</div>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row className="justify-content-center">
                    <Col lg={2}>
                        <div className="filter-table">
                            <div className="filter-header">
                                <div className="filter-title">FILTERS</div>
                                <button className="btn btn-clear-filter">
                                    CLEAR FILTERS
                                </button>
                            </div>
                            <div className="filter-body">
                                <div className="filter-box">
                                    <div className="filter-box-header">
                                        <span>Types</span>
                                    </div>
                                    <div className="filter-box-body">
                                        <div className="filter-list">
                                            <FilterCheckBoxItem
                                                title={'Electro'}
                                                titleColorClass={'t-violet'}
                                            ></FilterCheckBoxItem>
                                            <FilterCheckBoxItem
                                                title={'Dark'}
                                                titleColorClass={'t-dark'}
                                            ></FilterCheckBoxItem>
                                            <FilterCheckBoxItem
                                                title={'Acid'}
                                                titleColorClass={'t-yellow'}
                                            ></FilterCheckBoxItem>
                                            <FilterCheckBoxItem
                                                title={'Ice'}
                                                titleColorClass={'t-blue'}
                                            ></FilterCheckBoxItem>
                                            <FilterCheckBoxItem
                                                title={'Fire'}
                                                titleColorClass={'t-orange'}
                                            ></FilterCheckBoxItem>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={8}>
                        <ShipList col={2} dataListShip={shipData} limit={24} isShowPagination={true}></ShipList>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
