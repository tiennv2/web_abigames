import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Link from 'next/link';

export default function ShipDetail() {
    return (
        <div className="block block-ship-detail">
            <Container>
                <div className="block-header">
                    <div className="block-header-left">
                        <Link href="/">
                            <a className="btn btn-ship-detail btn-back">Back</a>
                        </Link>
                    </div>
                    <div className="block-header-right">
                        <a href="#" className="btn btn-ship-detail btn-buy">
                            Buy now
                        </a>
                    </div>
                </div>
                <div className="block-content">
                    <Row>
                        <Col lg={6}>
                            <div className="ship-summary">
                                <div className="ship-preview">
                                    <div className="ship-preview-row">
                                        <div className="ship-id">#555555</div>
                                        <div className="ship-medal">
                                            1ST WAVE
                                        </div>
                                    </div>
                                    <div className="ship-name">
                                        Electromagnetic Pulse
                                    </div>
                                    <div className="ship-preview-apartment">
                                        <Row>
                                            <Col lg={3}>
                                                <div className="ship-equip ship-equip-left">
                                                    <div className="ship-equip-box ship-equip-bullet">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                    <div className="ship-equip-box ship-equip-sp1">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                    <div className="ship-equip-box ship-equip-bullet">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={6}>
                                                <div className="ship-body">
                                                    <img src="/img/ship-detail/_full.png" />
                                                </div>
                                            </Col>
                                            <Col lg={3}>
                                                <div className="ship-equip ship-equip-right">
                                                    <div className="ship-equip-box ship-equip-shield">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                    <div className="ship-equip-box ship-equip-sp2">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                    <div className="ship-equip-box ship-equip-shield">
                                                        <div className="equip-hold"></div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                                <div className="ship-infor-panel">
                                    <div className="infor-panel-header">
                                        <div className="infor-panel-title">
                                            Precedents
                                        </div>
                                    </div>

                                    <Row>
                                        <Col lg={6}>
                                            <div className="infor-panel-content">
                                                <Row>
                                                    <Col lg={8}>
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-blue me-2">
                                                                    #375
                                                                </span>
                                                                <span className="t-red">
                                                                    Origin
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/ship.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Cold
                                                                            Fusion
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-sliver">
                                                                    Ship
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/ship.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Pulse
                                                                            Blast
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-sliver">
                                                                    Research
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/emps.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            3 /
                                                                            7
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col lg={4}>
                                                        <div className="ship-body">
                                                            <img src="/img/ships/ship1.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                        <Col lg={6}>
                                            <div className="infor-panel-content">
                                                <Row>
                                                    <Col lg={8}>
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-blue me-2">
                                                                    #375
                                                                </span>
                                                                <span className="t-red">
                                                                    Origin
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/ship.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Cold
                                                                            Fusion
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-sliver">
                                                                    Ship
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/ship.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Pulse
                                                                            Blast
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                <span className="t-sliver">
                                                                    Research
                                                                </span>
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/emps.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            3 /
                                                                            7
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col lg={4}>
                                                        <div className="ship-body">
                                                            <img src="/img/ships/ship2.png" />
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </Col>
                        <Col lg={6}>
                            <div className="ship-infor">
                                <div className="ship-infor-panel">
                                    <div className="infor-panel-header">
                                        <div className="infor-panel-title">
                                            About
                                        </div>
                                    </div>

                                    <div className="infor-panel-content">
                                        <Row>
                                            <Col lg={4}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Ship
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/ship.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold">
                                                                    Pulse Blast
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={4}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Element
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/shields.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold t-violet">
                                                                    Electro
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={4}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Research
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/emps.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold">
                                                                    5 / 7
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={12}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Owner
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/person.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title">
                                                                    <span className="t-bold">
                                                                        Player
                                                                        One
                                                                    </span>
                                                                    <span className="t-sliver">
                                                                        (0000x00000x000x0000x000x00)
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>

                                <div className="ship-infor-panel">
                                    <div className="infor-panel-header">
                                        <div className="infor-panel-title">
                                            Stats
                                        </div>
                                    </div>

                                    <div className="infor-panel-content">
                                        <Row>
                                            <Col lg={3}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Damage
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/weapons.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold">
                                                                    78
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={3}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Fire Rate
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/turrets.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold">
                                                                    42
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={3}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Armor
                                                    </div>
                                                    <div className="panel-box-header">
                                                        <div className="header-box-icon">
                                                            <img src="/img/ship-detail/armor.png" />
                                                        </div>
                                                        <div className="header-box-meta">
                                                            <div className="header-box-title t-bold">
                                                                56
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col lg={3}>
                                                <div className="panel-box">
                                                    <div className="panel-box-title">
                                                        Health
                                                    </div>
                                                    <div className="panel-box-content">
                                                        <div className="panel-box-header">
                                                            <div className="header-box-icon">
                                                                <img src="/img/ship-detail/power-plants.png" />
                                                            </div>
                                                            <div className="header-box-meta">
                                                                <div className="header-box-title t-bold">
                                                                    35
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>

                                <div className="ship-infor-panel">
                                    <div className="infor-panel-header">
                                        <div className="infor-panel-title">
                                            Part
                                        </div>
                                        <div className="infor-panel-subtitle">
                                            <ul className="rank-list">
                                                <li className="t-red">SS</li>
                                                <li className="t-violet">S</li>
                                                <li className="t-yellow">A</li>
                                                <li className="t-blue">B</li>
                                                <li className="t-green">C</li>
                                                <li className="t-orange">D</li>
                                                <li className="t-red-bold">
                                                    E
                                                </li>
                                                <li className="t-sliver">F</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <Row>
                                        <Col lg={12}>
                                            <div className="infor-panel-content">
                                                <Row>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Weapon
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/missile.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Military
                                                                        </div>
                                                                        <div className="header-box-subtitle t-yellow">
                                                                            Grade
                                                                            A
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="part-content">
                                                                    <div className="part-content-title">
                                                                        [1]
                                                                        Armor
                                                                        Buster
                                                                    </div>
                                                                    <div className="part-content-stats">
                                                                        <div className="part-stat part-stat-bullet">
                                                                            <span>
                                                                                76
                                                                            </span>
                                                                        </div>
                                                                        <div className="part-stat part-stat-icon">
                                                                            <img src="/img/ship-detail/shield_breaker.png" />
                                                                        </div>
                                                                        <div className="part-stat part-stat-shield">
                                                                            <span>
                                                                                42
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="part-desc">
                                                                        Fires
                                                                        high
                                                                        velocity
                                                                        railgun
                                                                        rounds
                                                                        that
                                                                        pierces
                                                                        through
                                                                        and
                                                                        reduces
                                                                        armor by
                                                                        10% on
                                                                        hit.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Wing
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/wing.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Civilian
                                                                        </div>
                                                                        <div className="header-box-subtitle t-green">
                                                                            Grade
                                                                            C
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="part-content">
                                                                    <div className="part-content-title">
                                                                        [1]
                                                                        Missile
                                                                        Barrage
                                                                    </div>
                                                                    <div className="part-content-stats">
                                                                        <div className="part-stat part-stat-bullet">
                                                                            <span>
                                                                                45
                                                                            </span>
                                                                        </div>
                                                                        <div className="part-stat part-stat-icon">
                                                                            <img src="/img/ship-detail/missile_barrage.png" />
                                                                        </div>
                                                                        <div className="part-stat part-stat-shield">
                                                                            <span>
                                                                                37
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="part-desc">
                                                                        After
                                                                        evolve
                                                                        burst,
                                                                        launches
                                                                        a
                                                                        barrage
                                                                        of 10
                                                                        homing
                                                                        missiles.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Armor
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/armor.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Stealth
                                                                        </div>
                                                                        <div className="header-box-subtitle t-blue">
                                                                            Grade
                                                                            B
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="part-content">
                                                                    <div className="part-content-title">
                                                                        [1]
                                                                        Defense
                                                                        matrix
                                                                    </div>
                                                                    <div className="part-content-stats">
                                                                        <div className="part-stat part-stat-bullet">
                                                                            <span>
                                                                                12
                                                                            </span>
                                                                        </div>
                                                                        <div className="part-stat part-stat-icon">
                                                                            <img src="/img/ship-detail/orbital_shield.png" />
                                                                        </div>
                                                                        <div className="part-stat part-stat-shield">
                                                                            <span>
                                                                                58
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="part-desc">
                                                                        Upon
                                                                        evolve
                                                                        burst,
                                                                        deploys
                                                                        a laser
                                                                        turret
                                                                        that
                                                                        shoots
                                                                        down
                                                                        incoming
                                                                        projectiles.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Core
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/core.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Industrial
                                                                        </div>
                                                                        <div className="header-box-subtitle t-orange">
                                                                            Grade
                                                                            D
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div className="part-content">
                                                                    <div className="part-content-title">
                                                                        [1]
                                                                        Power
                                                                        saver
                                                                    </div>
                                                                    <div className="part-content-stats">
                                                                        <div className="part-stat part-stat-bullet">
                                                                            <span>
                                                                                24
                                                                            </span>
                                                                        </div>
                                                                        <div className="part-stat part-stat-icon">
                                                                            <img src="/img/ship-detail/emergency_battery.png" />
                                                                        </div>
                                                                        <div className="part-stat part-stat-shield">
                                                                            <span>
                                                                                36
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div className="part-desc">
                                                                        Has 50%
                                                                        chance
                                                                        to aboid
                                                                        power
                                                                        loss on
                                                                        death.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Engine
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/thruster.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Racing
                                                                        </div>
                                                                        <div className="header-box-subtitle t-violet">
                                                                            Grade
                                                                            S
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                    <Col
                                                        className="part-col"
                                                        lg={3}
                                                    >
                                                        <div className="panel-box">
                                                            <div className="panel-box-title">
                                                                Tail
                                                            </div>
                                                            <div className="panel-box-content">
                                                                <div className="panel-box-header panel-box-header-part">
                                                                    <div className="header-box-icon">
                                                                        <img src="/img/ship-detail/part/tail.png" />
                                                                    </div>
                                                                    <div className="header-box-meta">
                                                                        <div className="header-box-title t-bold">
                                                                            Industrial
                                                                        </div>
                                                                        <div className="header-box-subtitle t-sliver">
                                                                            Grade
                                                                            F
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
