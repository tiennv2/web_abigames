import Layout from '../components/Layout';
import { AuthProvider } from '@/context/AuthContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import '/public/fonts/oswald/stylesheet.css';
import '/styles/globals.css';

// Import Swiper styles
import 'swiper/css';

function MyApp({ Component, pageProps }) {
    return (
        <AuthProvider>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </AuthProvider>
    );
}

export default MyApp;
