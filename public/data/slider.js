const dataSlider = [
    {
        image: "/images/banner.png",
    },
    {
        class:"img-right",
        image: "/images/advertising-1.svg",
    },
    {
        class:"img-left",
        image: "/images/advertising-2.svg",
    },
];

export default dataSlider;
