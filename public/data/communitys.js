const communityData = [
    {
        title: 'Telegram',
        count: '60K+',
        icon: '/img/icons/tele.svg',
        iconFooter: '/img/icons/telegram.webp',
    },
];

export default communityData;
