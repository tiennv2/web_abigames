const gunData = [
    {
        gunId: '1',
        name: 'Gun 1',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '2',
        name: 'Gun 2',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '3',
        name: 'Gun 3',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '4',
        name: 'Gun 4',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '5',
        name: 'Gun 5',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '6',
        name: 'Gun 6',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '7',
        name: 'Gun 7',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '8',
        name: 'Gun 8',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '9',
        name: 'Gun 9',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        gunId: '10',
        name: 'Gun 10',
        desc: "Chillington's passion is to become the best snowstar of Iceville",
    },
];

export default gunData;
