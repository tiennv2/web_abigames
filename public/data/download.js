const downloadData = [
    {
        title: 'IOS Download',
        icon: '/images/ios_download.png',
        link: '#',
    },
    {
        title: 'Google Play',
        icon: '/images/chplay_download.png',
        link: '#',
    },
    {
        title: 'Android Download',
        icon: '/images/android_download.png',
        link: '#',
    },
    {
        title: 'Plugin Wallet',
        icon: '/images/chrome_download.png',
        link: '#',
    },
    
];

export default downloadData;