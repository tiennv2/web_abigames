const rmData = [
    {
        quater: 'Sep 2021',
        tasks: [
            {
                title: 'Project Details',
                isComplete: true,
            },
            {
                title: 'Contract Development',
                isComplete: true,
            },
        ],
        status: 'Complete',
    },
    {
        quater: 'Oct 2021',
        tasks: [
            {
                title: 'Website Debut',
                isComplete: false,
            },
            {
                title: 'Angel/Seeding/Pre sale',
                isComplete: false,
            },
            {
                title: 'Public Sale',
                isComplete: false,
            },
            {
                title: 'Exchange Listing, Pancake/Uniswap Listing',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Nov 2021',
        tasks: [
            {
                title: 'Yield farming pools opening',
                isComplete: false,
            },
            {
                title: 'NFT testnet',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Dec 2021',
        tasks: [
            {
                title: 'Staking Opening',
                isComplete: false,
            },
            {
                title: 'Galaxy Attack: Revolution NFT sale',
                isComplete: false,
            },
            {
                title: 'Marketplace Phase 1',
                isComplete: false,
            },
            {
                title: 'TrustWallet, Metamask, Binance Wallet Integration',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Q1 2022',
        tasks: [
            {
                title: 'Galaxy Attack Game first Release V1.0',
                isComplete: false,
            },
            {
                title: 'ABI Bridge for Cross Chain NFT support',
                isComplete: false,
            },
            {
                title: 'Marketplace Phase 2',
                isComplete: false,
            },
            
        ],
        status: '',
    },
    {
        quater: 'Q2 2022',
        tasks: [
            {
                title: 'ABI Wallet',
                isComplete: false,
            },
            {
                title: 'Auction feature on Marketplace',
                isComplete: false,
            },
            {
                title: 'Expand Tier 1 for Legendary SHIP NFT',
                isComplete: false,
            },
            {
                title: 'New GAR Game features',
                isComplete: false,
            },
            {
                title: 'Listing on Major Exchange',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Q3 2022',
        tasks: [
            {
                title: 'Publish new Game',
                isComplete: false,
            },
            {
                title: 'GOV ABI DAO Voting Platform',
                isComplete: false,
            },
            {
                title: 'ABI Platform Mobile Application (iOS, Android)',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Q4 2022',
        tasks: [
            {
                title: 'Publish new Game',
                isComplete: false,
            },
            {
                title: 'Expanded Ecosystem',
                isComplete: false,
            },
        ],
        status: '',
    },
    {
        quater: 'Q1 2023',
        tasks: [
            {
                title: 'Expanded Ecosystem',
                isComplete: false,
            },
            {
                title: 'To Be Continued',
                isComplete: false,
            },
        ],
        status: '',
    },
];

export default rmData;
