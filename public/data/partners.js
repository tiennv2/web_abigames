const partnersData = [
    {
        image: '/img/partners/p1.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p2.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p3.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p5.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p8.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p9.png',
        link: '#',
        class: 'col-lg-4',
    },
    {
        image: '/img/partners/p10.png',
        link: '#',
        class: 'col-lg-6',
    },
    {
        image: '/img/partners/p12.png',
        link: '#',
        class: 'col-lg-6',
    },
];

export default partnersData;
