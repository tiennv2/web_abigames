const footerData = [
    {
        image: '/images/footer-1.svg',
        class: 'col-sm-4 col-md-4',
    },
    {
        image: '/images/footer-2.svg',
        class: 'col-sm-4 col-md-4',
    },
    {
        image: '/images/footer-3.svg',
        class: 'col-sm-4 col-md-4',
    },
    {
        image: '/images/footer-4.svg',
        class: 'last col-12',
    },
];

export default footerData;
