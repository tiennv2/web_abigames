const visionData = [
    {
        title: 'NFT',
        icon: '/images/NFT.png',
        link: '#',
        class: 'col-sm-4'
    },
    {
        title: 'GAME',
        icon: '/images/game.png',
        link: '#',
        class: 'col-sm-4'
    },
    {
        title: 'FINANCE',
        icon: '/images/finance.png',
        link: '#',
        class: 'col-sm-4'
    },
    {
        title: '',
        icon: '/images/vision.svg',
        link: '#',
        class: 'col-sm-12 col-12'
    },
    
];

export default visionData;