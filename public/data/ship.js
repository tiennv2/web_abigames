const shipData = [
    {
        shipId: '2',
        nameFirst: 'Tweets',
        nameLast: 'Tweetster',
        shipDesc: 'Tweetster is super charged & loves to ruffle his feathers as he soars the skies',
    },
    {
        shipId: '3',
        nameFirst: 'Snowie',
        nameLast: 'Sir Snowington',
        shipDesc: 'Sir Snowington has grown and evolved into a leader of the flock',
    },
    {
        shipId: '4',
        nameFirst: 'Bluey',
        nameLast: 'Bluessette',
        shipDesc: 'Bluessette is fun-loving & creative, she is the life of the party.',
    },
    {
        shipId: '5',
        nameFirst: 'Lei Lei',
        nameLast: 'Leilani',
        shipDesc: 'Leilani hulas her way through anything showing off moves.',
    },
    {
        shipId: '6',
        nameFirst: 'Mini Victory',
        nameLast: 'Victorious',
        shipDesc: 'Victorious leads his flocksters to the top',
    },
    {
        shipId: '7',
        nameFirst: 'Roosty',
        nameLast: 'Rooster',
        shipDesc: 'Rooster wakes the flock with his morning call helping them start their day',
    },
    {
        shipId: '8',
        nameFirst: 'Quack',
        nameLast: 'Quackers',
        shipDesc: 'Quackers is skilled quackanomics & teaches the rest of the twack his clever ways',
    },
    {
        shipId: '9',
        nameFirst: 'Flamingy',
        nameLast: 'Flaminglatina',
        shipDesc: 'Flaminglatina struts around showing off her tall and glamorous stature',
    },
    {
        shipId: '10',
        nameFirst: 'Chilly',
        nameLast: 'Chillington',
        shipDesc: "Chillington's passion is to become the best snowstar of Iceville",
    },
];

export default shipData;
