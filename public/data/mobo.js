const moboData = [
    {
        moboId: '42003',
        nameFirst: 'Ango',
        nameLast: 'Sheriff Angus',
        moboDesc: 'Sheriff Angus keeps the herd in line',
    },
    {
        moboId: '41001',
        nameFirst: 'Tweets',
        nameLast: 'Tweetster',
        moboDesc: 'Tweetster is super charged & loves to ruffle his feathers as he soars the skies',
    },
    {
        moboId: '41005',
        nameFirst: 'Snowie',
        nameLast: 'Sir Snowington',
        moboDesc: 'Sir Snowington has grown and evolved into a leader of the flock',
    },
    {
        moboId: '41006',
        nameFirst: 'Bluey',
        nameLast: 'Bluessette',
        moboDesc: 'Bluessette is fun-loving & creative, she is the life of the party.',
    },
    {
        moboId: '41008',
        nameFirst: 'Lei Lei',
        nameLast: 'Leilani',
        moboDesc: 'Leilani hulas her way through anything showing off moves.',
    },
    {
        moboId: '41009',
        nameFirst: 'Mini Victory',
        nameLast: 'Victorious',
        moboDesc: 'Victorious leads his flocksters to the top',
    },
    {
        moboId: '41010',
        nameFirst: 'Roosty',
        nameLast: 'Rooster',
        moboDesc: 'Rooster wakes the flock with his morning call helping them start their day',
    },
    {
        moboId: '41011',
        nameFirst: 'Quack',
        nameLast: 'Quackers',
        moboDesc: 'Quackers is skilled quackanomics & teaches the rest of the twack his clever ways',
    },
    {
        moboId: '41014',
        nameFirst: 'Flamingy',
        nameLast: 'Flaminglatina',
        moboDesc: 'Flaminglatina struts around showing off her tall and glamorous stature',
    },
    {
        moboId: '41015',
        nameFirst: 'Chilly',
        nameLast: 'Chillington',
        moboDesc: "Chillington's passion is to become the best snowstar of Iceville",
    },
    {
        moboId: '41017',
        nameFirst: 'Iggy',
        nameLast: 'Igloo',
        moboDesc: 'Igloo gets his name as he is the biggest & best at building homes in iceville',
    },
    {
        moboId: '42001',
        nameFirst: 'Oinkie',
        nameLast: 'Captain Oinksly',
        moboDesc: 'Captain Oinksly sails the high seas guiding his crew to safety',
    },
    {
        moboId: '42006',
        nameFirst: 'Pigsly',
        nameLast: 'Chef P.I.G',
        moboDesc: 'Chef P.I.G is famous for his cooking methods amoungst trottersviille',
    },
    {
        moboId: '42013',
        nameFirst: 'Cub Pooky',
        nameLast: 'Polar Pook',
        moboDesc: 'Polar Pook is the scout leader of iceville taking everyone on wild adventures',
    },
    {
        moboId: '43005',
        nameFirst: 'Sealee',
        nameLast: 'Admiral Sea Lion',
        moboDesc: 'Admiral Sea Lion sails the seas with his sailing crew',
    },
    {
        moboId: '43011',
        nameFirst: 'Little jabber',
        nameLast: 'Jabberjaws',
        moboDesc: 'Jabberjaws is cool calm & the strongest of the crew',
    },
    {
        moboId: '43012',
        nameFirst: 'Bree',
        nameLast: 'Breezer',
        moboDesc: 'Breezer surfs & Sails, catching waves hanging with all his surfer dudes',
    },
    {
        moboId: '43014',
        nameFirst: 'Sunny',
        nameLast: 'Sunny Ray',
        moboDesc: 'Sunny Ray is famous for his study of solar science',
    },
    {
        moboId: '43015',
        nameFirst: 'Pop',
        nameLast: 'El Capitan Popitto',
        moboDesc: 'El Capitan Popitto managers his crew with proficiency & is known to be the best Shfipmaster of the seas',
    },
    {
        moboId: '44001',
        nameFirst: 'Demmie',
        nameLast: 'Demon Dave',
        moboDesc: 'Demon Dave Pops in & out of corners gaining monster strength the more he frightens.',
    },
    {
        moboId: '44011',
        nameFirst: 'Jacko',
        nameLast: 'Jacko Senior',
        moboDesc: 'Jacko Senior loves to scare in the dark with his fiery lit up eyes',
    },
    {
        moboId: '44014',
        nameFirst: 'Leafatine',
        nameLast: 'Leafalicious',
        moboDesc: 'Leafalicious grows more powerful the more she blossoms',
    },
    {
        moboId: '44016',
        nameFirst: 'Kiki',
        nameLast: 'Kiktoro',
        moboDesc: 'Kiktoro loves to challenge all his monster friends to a scare match',
    },
    {
        moboId: '44017',
        nameFirst: 'Merly',
        nameLast: 'Merlin',
        moboDesc: 'Merlin the Great is the most famous magician in all of Monsters Town',
    },
];

export default moboData;
