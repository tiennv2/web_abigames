const aboutTabData = [
    {
        thumbnail: '/img/videos/galaxyattack-alienshooter.jpg',
        videoUrl: 'https://www.youtube.com/embed/9sQXNAkA6IE',
        title: 'Galaxy Attack: Alien Shooter',
    },
];

export default aboutTabData;
