const newsData = [
    {
        id: '01',
        title: 'Community Update #26',
        desc: 'PlayStation 5 - Xbox Series X/S - PlayStation 4 - Xbox One - PC',
        date: '2021-07-28',
        link: '#',
    },
    {
        id: '02',
        title: 'Community Update #25',
        desc: 'PlayStation 5 - Xbox Series X/S - PlayStation 4 - Xbox One - PC',
        date: '2021-07-28',
        link: '#',
    },
    {
        id: '03',
        title: 'MOBOX-Binance Research',
        desc: 'PlayStation 5 - Xbox Series X/S - PlayStation 4 - Xbox One - PC',
        date: '2021-07-28',
        link: '#',
    },
    {
        id: '04',
        title: 'NFT Yield Farming with a twist (DappRadar)',
        desc: 'PlayStation 5 - Xbox Series X/S - PlayStation 4 - Xbox One - PC',
        date: '2021-07-28',
        link: '#',
    },
    {
        id: '05',
        title: 'Introducing Mobox (MBOX) on Binance Launchpool!',
        desc: 'PlayStation 5 - Xbox Series X/S - PlayStation 4 - Xbox One - PC',
        date: '2021-07-28',
        link: '#',
    },
];

export default newsData;
