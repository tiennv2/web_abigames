import epicRobot from '../img/cmc/momo1.png';
import epicDanny from '../img/cmc/momo2.png';
import epicBobo from '../img/cmc/momo3.png';
import epicLoda from '../img/cmc/momo4.png';
import epicGoldorian from '../img/cmc/momo5.png';
import epicBino from '../img/cmc/momo6.png';
import epicDefault from '../img/cmc/momoq.209ff95c.png';

const epicData = [
    {
        title: 'CoinMarketCap Robot',
        desc: 'Meet Mr Roberto C.M.C Datatrom , He is flashy, he is Unique, he is super intelligent & always on the move! Watch him as he nuts through data, collaborates statistics & is up for for any challenge & ready to take on any of the MOMOs in the MOBOX Meterverse.',
        image: '/img/cmc/momo1.png',
        type: 'normal',
    },
    {
        title: 'Danny Dapporovsky',
        desc: "He is a unique & quick-witted number cruncher grasping all mathematical concepts and strategies quickly. He works through data & stats to compile the best outcome better than anyone in his field. Keep your eyes peeled for this numbers man he's ready to show off his sharp skills!",
        image: '/img/cmc/momo2.png',
        type: 'normal',
    },
    {
        title: 'Bobo Banana-Bot',
        desc: 'Deep in the Apeswaposhphere Jungle lies a rare simian primate. Half bot half mammal. His mighty monkey-like attributes help him swing from tree to tree as he swaps from bot to monkey in the blink of an eye. Collect this banana loving tree hugger for your chance to see him in action.',
        image: '/img/cmc/momo3.png',
        type: 'normal',
    },
    {
        title: 'Loda Team Alliance',
        desc: 'This unique guy is a famous E sports fan and takes Gaming to the next level. He is one of a kind and a leader in his pack. Collect this Rare Unique MOMO NFT for your chance to get a hold of a piece of E sports History!!',
        image: '/img/cmc/momo6.png',
        type: 'normal',
    },
    {
        title: 'Goldorian Smart Chainadore',
        desc: 'This unique feisty little guy blocks any unwanted predators from getting through to protect his community.Watch him flex in and out of ledgers of information adapting morphing and blocking any evil.',
        image: '/img/cmc/momo5.png',
        type: 'normal',
    },
    {
        title: 'Bino Binancian Bot',
        desc: 'He Is an engineered people pleaser with a tough outer chamber, helping charity is one of his sweeter roles. He is one of the main men in the Cryptosphere. With his most powerful features & attributes being that he is brisk and quick to rush & assist his realm & people with swift & easy exchanges & transactions',
        image: '/img/cmc/momo6.png',
        type: 'normal',
    },
    {
        title: 'Coming Soon',
        desc: '',
        image: '/img/cmc/momoq.209ff95c.png',
        type: 'default',
    },
    {
        title: 'Coming Soon',
        desc: '',
        image: '/img/cmc/momoq.209ff95c.png',
        type: 'default',
    },
];

export default epicData;
