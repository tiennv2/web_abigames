const ourGameData = [
    {
        title: 'MOMO Farmer',
        subtitle: 'Farm and Earn!',
        imageBg: '/img/posts/post-bg-1.jpg',
        imagetitle: '/img/posts/post-title-1.png',
        link: 'GO>',
    },
    {
        title: 'MOMO:Token Master',
        subtitle: 'Twirl and Win!Loot and Win!',
        imageBg: '/img/posts/post-bg-2.jpg',
        imagetitle: '/img/posts/post-title-2.png',
        link: 'GO>',
    },
    {
        title: 'Trade Action',
        subtitle: 'Learn, Earn, Compete!',
        imageBg: '/img/posts/post-bg-3.jpg',
        imagetitle: '/img/posts/post-title-3.png',
        link: 'GO>',
    },
    {
        title: 'MOMO:Block Brawler',
        subtitle: 'Bash, Break, and Brawl!',
        imageBg: '/img/posts/post-bg-4.jpg',
        imagetitle: '/img/posts/post-title-4.png',
        link: 'Comming Soon',
    },
    {
        title: 'MOMO:Tactical Party Slaye',
        subtitle: `Slay em' Slash em'`,
        imageBg: '/img/posts/post-bg-5.jpg',
        imagetitle: '/img/posts/post-title-5.png',
        link: 'Comming Soon',
    },
];

export default ourGameData;
