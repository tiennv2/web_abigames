const adverData = [
    {
        image: '/images/no1.svg',
        title: 'NO 1',
        desc: 'MBOX Listed On Binance LaunchPool',
        name: 'GAMEGAR',
    },
    {
        image: '/images/nft-trade.svg',
        title: '$183M+',
        desc: 'NFT Trades',
        name: 'NFT FAMER',
    },
    {
        image: '/images/cup.svg',
        title: '320K+',
        desc: 'Player',
        name: 'COMMUNITY',
    },
];

export default adverData;
