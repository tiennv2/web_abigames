if ($(window).width() <= 1200) {
    $(".header .menu-bars-mobile, .header .navbar ul li ").click(function(){
        $(".header nav").slideToggle();
    });
}

var canvas = document.getElementById("snowfall");
    var contex = canvas.getContext('2d'),
        
    cnsWidth = window.innerWidth,
    cnsHeight = window.innerHeight,
    numberFlakes = 100,
    flakes = [];

    function Flake(x, y) {
        var topSize = 4.5, topMotion = 2.5;
        
        this.x = x;
        this.y = y;
        this.spacing = spaceBetween(0, 0.8);
        this.distance = spaceBetween(0, Math.PI);
        this.weight = spaceBetween(2, topSize);
        this.scale = (this.weight / topSize);
        this.motion = (this.weight / topSize) * topMotion; 
            
        this.update = function() {
            this.x += Math.cos(this.distance) * this.spacing;   
            this.y += this.motion;
        }  
    }

    function init() {
        var i = numberFlakes;
            
        while (i--) {
            x = spaceBetween(0, cnsWidth, true);
            y = spaceBetween(0, cnsHeight, true);
                
            flake = new Flake(x, y);
            flakes.push(flake);
        }
        
        scaleCanvas();
        loop();  
    }

    function scaleCanvas() {
        canvas.width = cnsWidth;
        canvas.height = cnsHeight;
    }

    function loop() {
        var i = flakes.length;
        
        contex.save();
        contex.setTransform(1, 0, 0, 1, 0, 0);
        contex.clearRect(0, 0, cnsWidth, cnsHeight);
        contex.restore();
        
        while (i--) {    
            flakeFront = flakes[i];
            flakeFront.update();    
            contex.beginPath();
            contex.arc(flakeFront.x, flakeFront.y, flakeFront.weight, 0, 2 * Math.PI, false);
            contex.fillStyle = "#52bfe6"; + flakeFront.scale + ')';
            contex.fill();
            
            if (flakeFront.y >= cnsHeight) {
                flakeFront.y = -flakeFront.weight;
            }  
        }  
        requestAnimationFrame(loop);
    }

    function spaceBetween(min, max, round) {
        var number = Math.random() * (max - min + 1) + min;

        if (round) {
            return Math.floor(number);
        } else {
            return number;
        }
    }

    init();